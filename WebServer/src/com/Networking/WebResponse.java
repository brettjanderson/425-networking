package com.Networking;

import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by Brett Anderson on 2/6/15.
 */
public class WebResponse {

    private String httpVersion;
    private int statusCode;
    private String reasonPhrase;
    private PrintWriter outStream;
    private Scanner messageScanner;

    public WebResponse(String httpVersion, int statusCode, String reasonPhrase, Scanner messageResource){
        this.messageScanner = messageResource;
        this.httpVersion = httpVersion;
        this.statusCode = statusCode;
        this.reasonPhrase = reasonPhrase;
    }

    public void sendResponse(PrintWriter outStream){
        this.outStream = outStream;

        outStream.println(httpVersion +" "+ statusCode +" "+ reasonPhrase);
        outStream.println();

        while(messageScanner.hasNextLine()){
            outStream.println(messageScanner.nextLine());
        }

    }

}

