package com.Networking;
import java.io.*;
import java.net.*;

public class WebServer {
    public static void main(String[] args){

        int portNumber = 8080;

        if(args.length==1)
            portNumber = Integer.parseInt(args[0]);
        
        try(ServerSocket listener = new ServerSocket(portNumber))
        {
            while(true){
                System.out.println("Listening on port: "+portNumber);
                Socket connection = listener.accept();
                System.out.println("Serving");
                (new Thread(new WebConnection(connection))).start();
            }
        } catch (IOException e){
            System.err.println("Could not listen on port " + portNumber);
            System.exit(-1);
        }
    }
}
