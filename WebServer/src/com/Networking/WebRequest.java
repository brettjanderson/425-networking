package com.Networking;

import java.util.StringTokenizer;

/**
 * this class takes the first line of request and 
 * separates it into strings method, uri, and httpVersion
 * 
 * @author Brett & Ehsan
 */
public class WebRequest {

    private StringTokenizer tokenizer;
    private String method = null;
    private String uri = null;
    private String httpVersion = null;

    public WebRequest(String firstLine) {

        if(firstLine != null) {
            tokenizer = new StringTokenizer(firstLine);

            if (tokenizer.hasMoreTokens())
                method = tokenizer.nextToken();
            if (tokenizer.hasMoreTokens())
                uri = tokenizer.nextToken();
            if (tokenizer.hasMoreTokens())
                httpVersion = tokenizer.nextToken();

            if(uri.equals("/")){
                uri = "/index.html";
            }
        }
    }

    public String getMethod() {
        return method;
    }

    public String getUri() {
        return uri;
    }

    public String getHttpVersion() {
        return httpVersion;
    }
}

