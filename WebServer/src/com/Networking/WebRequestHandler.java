package com.Networking;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Brettness on 2/6/15.
 */
public class WebRequestHandler {
    private static WebRequestHandler ourInstance = null;

    public static WebRequestHandler getInstance() {
    	if(ourInstance == null)
    		ourInstance = new WebRequestHandler();
        return ourInstance;
    }

    private WebRequestHandler() {
    }

    public synchronized static WebResponse processRequest(WebRequest request) throws FileNotFoundException {

        if(request == null){
            return new WebResponse("HTTP/1.1",400, "Bad Request", new Scanner("<h1>400: Bad Request</h1>"));
        }

        if(!request.getMethod().equals("GET")){
            return new WebResponse("HTTP/1.1",501, "Not Implemented", new Scanner("<h1>501: The requested service is not implemented</h1>"));
        }

        if(request.getUri().contains("..")){
            return new WebResponse("HTTP/1.1",400, "Bad Request", new Scanner("<h1>400: Bad Request</h1>"));
        }

        File toTransmit;
        toTransmit = new File("www/" + request.getUri().substring(1));

        Scanner responseResource;
        try{
            responseResource = new Scanner(toTransmit);
        } catch (FileNotFoundException e){
            return new WebResponse("HTTP/1.1",404, "File Not Found", new Scanner("<h1>404: File Not Found</h1>"));
        }

        return new WebResponse("HTTP/1.1", 200, "OK", responseResource);
    }
}
