package com.Networking;

import java.net.*;
import java.io.*;

public class WebConnection implements Runnable{
    private Socket socket = null;
    
    public WebConnection(Socket socket){
        this.socket = socket;   
        System.out.println("Local host: "+this.socket.getLocalAddress());
        System.out.println("Local port: "+this.socket.getLocalPort());
        System.out.println("Remote host: "+this.socket.getInetAddress());
        System.out.println("Remote port: "+this.socket.getPort());
    }
    
    public void run(){
        try(PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))){
            String request = in.readLine();
            if(request == null){
                socket.close();
            } else {
                WebRequest webRequest = new WebRequest(request);
                WebResponse webResponse = WebRequestHandler.processRequest(webRequest);
                webResponse.sendResponse(out);
                socket.close();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    
}

